# Unitree Go1 Digital Twin service deployment configurations

## What is this?

This repository contains different deployment configurations for the Unitree Go1 Digital Twin service using `docker-compose`.

> Note: Integration with Kubernetes is currently under development.






