#!/bin/bash

################################################################################
# Script Description:
# This script is designed to automate the configuration of an existing 
# Kubernetes cluster. It uses Helm, a package manager for Kubernetes, to 
# deploy services, config-maps, and deployments.
#

# Usage:
# Ensure you are inside the 'unitree-go1-digital-twin/digital-twin-service/chart/' directory 
# before executing.
# ./apply-k8s-config.sh
################################################################################

# Uninstall the Helm releases if they exist
helm uninstall "app-services" > /dev/null 2>&1 || true
helm uninstall "app-core" > /dev/null 2>&1 || true

# This function validates the format of an IP address.
validate_ip() {
    local ip=$1
    local ip_regex="^([0-9]{1,3}\.){3}[0-9]{1,3}$"

    # Check if the IP matches the regular expression.
    if [[ $ip =~ $ip_regex ]]; then
        # Split the IP into octets and check each one.
        IFS='.' read -ra octets <<< "$ip"
        for octet in "${octets[@]}"; do
            # If an octet is out of range (0-255), it's an invalid IP.
            if ((octet < 0 || octet > 255)); then
                echo "Invalid IP address: $ip"
                exit 1  # Exit the script if IP is invalid
            fi
        done
    else
        echo "Invalid IP address: $ip"
        exit 1  # Exit the script if IP is invalid
    fi
}

# Try to install the services using Helm
# If the installation fails, print an error message and exit the script.
if helm install app-services ./app \
  -f ./app/values/service-values.yaml; then
    echo "Services were applied successfully."
else
    echo "Error: Failed to apply services. Please review the configuration and attempt the installation again."
    exit 1
fi


# Try to apply the Helm installation for config-maps and deployments.
# If the installation fails, print an error message and exit the script.
if helm install app-core ./app \
  -f ./app/values/config-map-values.yaml \
  -f ./app/values/deployment-values.yaml \
    echo "Configmaps, and deployments were applied successfully."
else
    echo "Error: Failed to apply either secrets, configmaps, or deployments. Please review the configuration and attempt the installation again."
    exit 1
fi

# Print a success message when the configuration is completed.
echo ""
echo "Configuration completed successfully."
